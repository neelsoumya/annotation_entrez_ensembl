# annotation_entrez_ensembl

	This is an R script to convert Entrez Gene
	IDs to Ensembl Gene IDs
 
 Installation:

	 ## try http:// if https:// URLs are not supported

 	source("https://bioconductor.org/biocLite.R")

 	biocLite("org.Hs.eg.db") 
 
 Usage:

  	R --no-save < annotation_entrez_ensembl.R	

Acknowledgements:

       Kevin Rue-Albrecht and Maria Jose Gomez


