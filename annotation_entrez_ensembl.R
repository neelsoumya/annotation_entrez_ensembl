######################################################
# This is an R script to convert Entrez Gene
#	IDs to Ensembl Gene IDs
# 
# Installation:
# ## try http:// if https:// URLs are not supported
# source("https://bioconductor.org/biocLite.R")
# biocLite("org.Hs.eg.db") 
# 
# Usage:
# nohup R --no-save < annotation_entrez_ensembl.R	
#
# Acknowledgements:
#	Kevin Rue-Albrecht and Maria Jose Gomez
#
######################################################

##########################
# Load library
##########################
library(sqldf)
library(org.Hs.eg.db)


#############################################
# Annotate from Entrez Gene IDs to Ensembl
#############################################
AnnotationDbi::select(x = org.Hs.eg.db, keys = "4519", columns = c("GENENAME", "ENSEMBL"))

# Extra code to do in a for lopp give a data frame of Entrez Gene IDs
#for (str_temp_entrez_gene_id in list_entrez_gene_ids$gene_id)
#{
#    cat(str_temp_entrez_gene_id, "\n")
#AnnotationDbi::select(x = org.Hs.eg.db, keys = (str_temp_entrez_gene_id), columns = c("GENENAME", "ENSEMBL"))

#df_temp_entrez = try( ( AnnotationDbi::select(x = org.Hs.eg.db, keys = c(str_temp_entrez_gene_id), columns = c("GENENAME", "ENSEMBL")) ),
 #                         silent = TRUE)
  #  if ( inherits(df_temp_entrez, 'try-error') )
  #  {
  #        #<error handling statement>
  #        list_entrez_ids = rbind(list_entrez_ids, "")
          
  #  }
  #  else{
  #    list_entrez_ids = rbind(list_entrez_ids, df_temp_entrez$ENSEMBL[1])
  #  }

#}


